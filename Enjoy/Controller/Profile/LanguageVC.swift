//
//  LanguageVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH

class LanguageVC: UIViewController {

    @IBOutlet weak var englishButton: DesignableButton!
    @IBOutlet weak var arabicButton: DesignableButton!
    @IBOutlet weak var okButton: DesignableButton!

    var language: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func chooseEnglish(_ sender: DesignableButton) {
        sender.isEnabled = false
        okButton.backgroundColor = #colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1)
        sender.rightHandImage = #imageLiteral(resourceName: "ChoosenTick")
        sender.rightImageSize = CGSize(width: 16.9, height: 12.7)
        if (arabicButton.subviews.count > 1) {
            arabicButton.subviews[1].removeFromSuperview()
            arabicButton.subviews[1].removeFromSuperview()
            }
        arabicButton.isEnabled = true
        language = "en"
    }
    
    @IBAction func chooseArabic(_ sender: DesignableButton) {
        sender.isEnabled = false
        okButton.backgroundColor = #colorLiteral(red: 0.8235294118, green: 0.1921568627, blue: 0.5098039216, alpha: 1)
        sender.rightHandImage = #imageLiteral(resourceName: "ChoosenTick")
        sender.rightImageSize = CGSize(width: 16.9, height: 12.7)
        if (englishButton.subviews.count > 1) {
        englishButton.subviews[1].removeFromSuperview()
        englishButton.subviews[1].removeFromSuperview()
        }
        englishButton.isEnabled = true
        language = "ar"
    }
    
    @IBAction func chooseLangauge(_ sender: DesignableButton) {
        if language.isEmpty {
            enjoyToast("You must choose a language".localized)
            return
        }
        MOLH.setLanguageTo(language)
        save(true, Constants.openedEnjoyBefore)
        self.openVC(storyBoard: "Authentication", identifier: "AuthVC")
    }
}
