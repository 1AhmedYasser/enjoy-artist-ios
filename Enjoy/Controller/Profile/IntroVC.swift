//
//  IntroVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/12/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class IntroVC: UIViewController, UIScrollViewDelegate {
    
        @IBOutlet weak var scrollView: UIScrollView!
        @IBOutlet weak var pageControl: UIPageControl!
       
        @IBOutlet weak var getStaredButton: UIButton!
        
        var slides:[Slide] = [];
         
        override func viewDidLoad() {
        
            super.viewDidLoad()
            slides = createSlides()
            setupSlideScrollView(slides: slides)
            pageControl.numberOfPages = slides.count
            pageControl.currentPage = 0
            view.bringSubviewToFront(pageControl)
            scrollView.contentSize.height = 1.0
        }
    
        override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            setupSlideScrollView(slides: slides)
        }
    
        func createSlides() -> [Slide] {
            
            let slide1 = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
            slide1.setupSlide(image: #imageLiteral(resourceName: "intro5"), title: "You are the messenger of love\nfor thousands", description: "they choose you from the world to bear their\nfeelings and pulse .Their hearts to be\nable to send to those they love.")
            
            let slide2 = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
            slide2.setupSlide(image: #imageLiteral(resourceName: "intro6"), title: "Let your voice & your feelings", description: "be their Feel it and be close to them")
    
            let slide3 = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
            slide3.setupSlide(image: #imageLiteral(resourceName: "intro4"), title: "Enjoy is a sincere link between", description: "pure hearts, share joys and be comforted\nto them in sorrows.")
            
            return [slide1, slide2, slide3]
        }
        
        func setupSlideScrollView(slides : [Slide]) {
            scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
            scrollView.isPagingEnabled = true
            
            for i in 0 ..< slides.count {
                slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
                scrollView.addSubview(slides[i])
            }
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
            pageControl.currentPage = Int(pageIndex)
        }
}

class  Slide: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var introDescription: UILabel!
    
    func setupSlide(image: UIImage,title: String,description: String) {
        self.imageView.image = image
        self.title.text = title
        self.introDescription.text = description
    }
}
