//
//  ArtistDetailsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/25/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import Kingfisher
import MOLH
import NVActivityIndicatorView

class ArtistProfileVC: UIViewController, ArtistCellDelegate, ableToReceiveData {
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    var artist: ArtistInfoData?
    
    func passedData(data: String) {
        if data.isEmpty == false {
            print("Video Path is \(data)")
            //uploadPromoVideo(promo: data)
            uploadPromoVideo(promo: convertVideoToBase64(URL(fileURLWithPath: data)))
        } else {
            print("Empty Video Path")
        }
    }
    
    @IBOutlet weak var artistImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistVideosNo: UILabel!
    @IBOutlet weak var artistOrdersNo: UILabel!
    @IBOutlet weak var artistPayments: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var promoButton: DesignableButton!
    @IBOutlet weak var submitVidButton: DesignableButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var pickerController =  UIImagePickerController()
    var insertVideos = true
    var profileVideos =  [ProfileVideo]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.makeNavTransparent(nav: self.navigationController!)
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.movie"]
        self.pickerController.videoQuality = .typeHigh
        
//        do {
//            if let data = UserDefaults.standard.data(forKey: Constants.userProfileVideos) {
//                self.profileVideos = try PropertyListDecoder().decode([ProfileVideo].self, from: data)
//            }
//        } catch {
//            print("this is Exeception")
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        //  var observerToken:NSKeyValueObservation?
        _ = UserDefaults.standard.observe(\.notificationCount, options: [.initial, .new], changeHandler: { (defaults, change) in
            print("ChAnGeD")
        })
        
        if self.navigationController!.navigationBar.isTranslucent == false {
            makeNavTransparent(nav: self.navigationController!)
        }
        
        loadArtistInfo()
        loadArtistData(id: EnjoyData.shared.getIntValue(Constants.userId))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func loadArtist() {
       
    }
    
    func loadArtistData(id: Int) {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest("\(NConstants.artist)/\(id)",clients: false, ArtistInfo.self,isHeaders: true) { (user, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if user!.success == true {
//                    if let data = try? PropertyListEncoder().encode(user?.data.profile_videos) {
//                        UserDefaults.standard.set(data, forKey: Constants.userProfileVideos)
//                    }
//                    if let data = UserDefaults.standard.data(forKey: Constants.userProfileVideos) {
//                        self.profileVideos = try! PropertyListDecoder().decode([ProfileVideo].self, from: data)
//                    }
                    //self.insertVideos = false
                    self.artist = user?.data
                    self.profileVideos = (user?.data.profile_videos)!
                   // self.collectionView.reloadData()
                    
                } else {
                    self.enjoyToast(user!.message)
                }
            }
        }
    }
    
    fileprivate func loadArtistInfo() {
        
        if EnjoyData.shared.getStringValue(Constants.userImage).isEmpty {
            artistImage.image = #imageLiteral(resourceName: "Guest")
        } else {
            artistImage!.kf.setImage(with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        }
        
        if MOLHLanguage.currentAppleLanguage() == "en" {
            artistName.text = EnjoyData.shared.getStringValue(Constants.userEnglishName)
            promoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
            submitVidButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        } else {
            artistName.text = EnjoyData.shared.getStringValue(Constants.userArabicName)
            promoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
            submitVidButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        }
        
        artistVideosNo.text = EnjoyData.shared.getStringValue(Constants.userVideosNo)
        artistOrdersNo.text = EnjoyData.shared.getStringValue(Constants.userOrdersNo)
        artistPayments.text = EnjoyData.shared.getStringValue(Constants.userPayments)
        
    }
    
    @IBAction func submitVideo(_ sender: DesignableButton) {
        openVC(storyBoard: "Menu", identifier: "VideoDetailsVC")
    }
    
    @IBAction func submitPromo(_ sender: DesignableButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //        if let action = self.action(for: .camera, title: "Take video") {
        //            alertController.addAction(action)
        //        }
        alertController.addAction(UIAlertAction(title: "Take Video", style: .default) { (action) in
            let arViewController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "ARViewController") as! ARViewController
            arViewController.delegate = self
            arViewController.modalPresentationStyle = .overCurrentContext
            self.present(arViewController, animated: true, completion: nil)
            
        })
        
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Video library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true)
    }
    
    func fullScreenPressed(cell: ArtistVideoCell) {
        if cell.player != nil {
            let playerViewController = AVPlayerViewController()
            playerViewController.player = cell.player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    private func pickerController(_ controller: UIImagePickerController, didSelect url: URL?) {
        controller.dismiss(animated: true, completion: nil)
        uploadPromoVideo(promo: convertVideoToBase64(url!))
    }
    
    func uploadPromoVideo(promo: String) {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.uploadPromo, PromoVideo.self,["promo_video":promo],isHeaders: true,.post) { (promo, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if promo!.success == true {
                    self.enjoyToast(promo!.message)
                    self.save(promo?.data.promo_video, Constants.userPromo)
                    self.insertVideos = false
                    self.collectionView.reloadData()
                } else {
                    self.enjoyToast(promo!.message)
                }
            }
        }
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.present(self.pickerController, animated: true)
        }
    }
}

extension ArtistProfileVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if EnjoyData.shared.getStringValue(Constants.userPromo).isEmpty == true && self.artist == nil {
            return 0
        } else {
            return profileVideos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let videoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtistVideoCell", for: indexPath) as! ArtistVideoCell
        videoCell.delegate = self
        videoCell.setupVideo(video: profileVideos[indexPath.row],insert: insertVideos)
        return videoCell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ArtistPromoHeader", for: indexPath) as! ArtistPromoHeader
        headerCell.setupVideo(video: EnjoyData.shared.getStringValue(Constants.userPromo),insert: insertVideos)
        return headerCell
    }
}

extension ArtistProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let url = info[.mediaURL] as? URL else {
            return self.pickerController(picker, didSelect: nil)
        }
        
        //        //uncomment this if you want to save the video file to the media library
        //                if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path) {
        //                    UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, nil, nil)
        //                }
        print(url)
        self.pickerController(picker, didSelect: url)
    }
}

protocol ableToReceiveData {
    func passedData(data: String)
}
