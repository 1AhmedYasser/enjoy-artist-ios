//
//  ChangePasswordVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/20/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var oldPassword: DesignableUITextField!
    @IBOutlet weak var newPassword: DesignableUITextField!
    @IBOutlet weak var confirmPassword: DesignableUITextField!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           if MOLHLanguage.currentAppleLanguage() == "ar" {
               let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
               backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               self.navigationItem.leftBarButtonItem  = backButton
           }
       }
    
    @IBAction func changePassword(_ sender: UIButton) {
        
        if oldPassword.text!.isEmpty || newPassword.text!.isEmpty || confirmPassword.text!.isEmpty {
            enjoyToast("Please fill the empty fields".localized)
            return
        } else {
            sender.isEnabled = false
            loadingIndicator.startAnimating()
            let parameters = ["old_password":oldPassword.text!,"new_password":newPassword.text!,"confirm_new_password":confirmPassword.text!]
            EnjoyAPI.EnjoyRequest(NConstants.changePassword,EnjoyError.self,parameters,.post) { (message, errorMessage, error) in
                self.loadingIndicator.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                   self.enjoyToast(errorMessage)
                } else {
                    if message!.success == true {
                    self.goBack(sender)
                    } else {
                    self.enjoyToast(message!.message)
                    }
                }
            }
        }
    }
}
