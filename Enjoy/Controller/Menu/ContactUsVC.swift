//
//  ContactUsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {
    
    @IBOutlet weak var phoneNumber: DesignableButton!
    @IBOutlet weak var email: DesignableButton!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumber.setTitle(EnjoyData.shared.getStringValue(Constants.contactPhoneNumber), for: .normal)
        email.setTitle(EnjoyData.shared.getStringValue(Constants.contactEmail), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        email.titleLabel!.minimumScaleFactor = 0.5;
        email.titleLabel!.adjustsFontSizeToFitWidth = true;
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
    }
    
    @IBAction func openSocialLink(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.facebookLink))
        case 1:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.twitterLink))
        case 2:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.youtubeLink))
        case 3:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.instagramLink))
        case 4:
            openUrl(url: EnjoyData.shared.getStringValue(Constants.snapchatLink))
        default:
            print("Error")
        }
    }
    
    func openUrl(url: String) {
        if let url = URL(string: url) {
           if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            } else {
                print("Cant open Url")
            }
        }
    }
}
