//
//  SettingsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/19/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import Kingfisher
import NVActivityIndicatorView

class SettingsVC: UIViewController {
    
    @IBOutlet weak var userImage: DesignableImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var englishButton: DesignableButton!
    @IBOutlet weak var arabicButton: DesignableButton!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty == false {
            //    loadUserInfo()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        loadUserInfo()
        if self.navigationController!.navigationBar.isTranslucent {
            applyEnjoyGradient(nav: self.navigationController!)
        }
    }
    
    func loadUserInfo() {
        (MOLHLanguage.currentAppleLanguage() == "en") ? (userName.text = EnjoyData.shared.getStringValue(Constants.userEnglishName)) : (userName.text = EnjoyData.shared.getStringValue(Constants.userArabicName))
        userEmail.text = EnjoyData.shared.getStringValue(Constants.userEmail)
        userImage.kf.setImage(
            with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage)),
            placeholder: userImage.image,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
        ])
        
        MOLHLanguage.currentAppleLanguage() == "en" ? changeLanguageState(sender: englishButton,other: arabicButton) : changeLanguageState(sender: arabicButton,other: englishButton)
    }
    
    @IBAction func changeLanguage(_ sender: DesignableButton) {
        (sender.titleLabel?.text == "English") ? MOLH.setLanguageTo("en") : MOLH.setLanguageTo("ar")
        MOLH.reset()
//        if #available(iOS 13.0, *) {
//            let delegate = UIApplication.shared.delegate as? AppDelegate
//            delegate!.swichRoot()
//        } else {
//            MOLH.reset()
//        }
    }
    
    func changeLanguageState(sender: DesignableButton,other: DesignableButton) {
        sender.isEnabled = false
        sender.setTitleColor(#colorLiteral(red: 0.368627451, green: 0.07450980392, blue: 0.4039215686, alpha: 1), for: .normal)
        sender.rightHandImage = #imageLiteral(resourceName: "ChoosenTick")
        sender.rightImageSize = CGSize(width: 16.9, height: 12.7)
        if (other.subviews.count > 1) {
            other.subviews[1].removeFromSuperview()
            other.subviews[1].removeFromSuperview()
        }
        other.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        other.isEnabled = true
    }
    
    @IBAction func editProfile(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        makeNavTransparent(nav: self.navigationController!)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        openVC(storyBoard: "Menu", identifier: "ChangePasswordVC")
    }
    
    @IBAction func logout(_ sender: UIButton) {
        logout(sender: sender,loadingIndicator: loadingIndicator)
    }
}
