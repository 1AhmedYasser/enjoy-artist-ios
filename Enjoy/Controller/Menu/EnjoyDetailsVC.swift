//
//  EnjoyDetailsVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class EnjoyDetailsVC: UIViewController {
    
    var isTerms: Bool = false

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 257
        
        if isTerms {
            tableView.estimatedRowHeight = 85
            self.navigationController!.isNavigationBarHidden = false
        } else {
            tableView.estimatedRowHeight = 257
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
    }
}

extension EnjoyDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isTerms == false {
        let aboutUsCell = tableView.dequeueReusableCell(withIdentifier: "AboutUsCell", for: indexPath) as! EnjoyDetailsCell
            aboutUsCell.setupCell(text: EnjoyData.shared.getStringValue(Constants.aboutApp))
        
        return aboutUsCell
        } else {
            let termsCell = tableView.dequeueReusableCell(withIdentifier: "TermsCell", for: indexPath) as! EnjoyDetailsCell
            termsCell.setupCell(text: EnjoyData.shared.getStringValue(Constants.termsConditions))
            return termsCell
        }
    }
}
