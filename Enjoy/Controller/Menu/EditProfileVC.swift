//
//  EditProfileVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/20/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import Kingfisher
import NVActivityIndicatorView
import MOLH

class EditProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var userImage: DesignableImageView!
    @IBOutlet weak var englishName: DesignableUITextField!
    @IBOutlet weak var arabicName: DesignableUITextField!
    @IBOutlet weak var email: DesignableUITextField!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
    }
    
    func loadUserData() {
        userImage.kf.setImage(
            with: URL(string: EnjoyData.shared.getStringValue(Constants.userImage)),
            placeholder: userImage.image,
            options: [
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
        ])
        
        englishName.text = EnjoyData.shared.getStringValue(Constants.userEnglishName)
        arabicName.text = EnjoyData.shared.getStringValue(Constants.userArabicName)
        email.text = EnjoyData.shared.getStringValue(Constants.userEmail)
    }
    
    @IBAction func chooseImage(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Set Profile Image".localized, message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Take a picture".localized, style: .default, handler: { action  in
            self.initializeImagePicker(source: .camera)
        }))
        
        
        alert.addAction(UIAlertAction(title: "Choose from gallery".localized, style: .default, handler: {action in
            self.initializeImagePicker(source: .photoLibrary)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
    
    @IBAction func updateUserInfo(_ sender: UIButton) {
        sender.isEnabled = false
        if englishName.text!.isEmpty || email.text!.isEmpty || arabicName.text!.isEmpty {
            enjoyToast("Please fill the empty fields".localized)
            sender.isEnabled = true
            return
        }
        
        if isValidEmail(email) == false {
            enjoyToast("Your Email Address is not valid".localized)
            sender.isEnabled = true
            return
        }
        
        let parameters = ["id": EnjoyData.shared.getIntValue(Constants.userId),
                          "name_en": englishName.text ?? "",
                          "name_ar": arabicName.text ?? "",
                          "email": email.text ?? "",
                          "image": "\(convertImageToBase64(userImage.image!))"] as [String : Any]
        loadingIndicator.startAnimating()
        
        EnjoyAPI.EnjoyRequest(NConstants.editProfile,User.self,parameters,isHeaders: true,.post) { (user, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            sender.isEnabled = true
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if user!.success == true {
                    EnjoyData.shared.SaveUserInfo(user: user?.data,withToken: false)
                    self.goBack(sender)
                } else {
                    self.enjoyToast(user!.message)
                }
            }
        }
    }
    
    func initializeImagePicker(source: UIImagePickerController.SourceType){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source
        imagePicker.allowsEditing = true
        present(imagePicker,animated: true,completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let retrievedImage = info[.editedImage] as? UIImage {
            userImage.image = retrievedImage
        } else if let retrievedImage = info[.originalImage] as? UIImage {
            userImage.image = retrievedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}
