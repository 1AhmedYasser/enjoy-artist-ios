//
//  EnjoyAlert.swift
//  Enjoy
//
//  Created by Ahmed yasser on 1/13/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import Foundation
import UIKit

class EnjoyPopup: UIView {
    
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var alertMessage: UILabel!
    var viewController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupAlert(image: UIImage,message: String,vc: UIViewController) {
        self.alertImage.image = image
        self.alertMessage.text = message
        self.viewController = vc
    }
    
    class func instantiateFromNib() -> EnjoyPopup {
        return Bundle.main.loadNibNamed("EnjoyAlert", owner: nil, options: nil)!.first as! EnjoyPopup
    }
    
    @IBAction func dismissAlert(_ sender: UIButton) {
        viewController.dismiss(animated: true, completion: nil)
    }
}
