//
//  LoginRequestVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class LoginRequestVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func goToLogin(_ sender: Any) {
        openVC(storyBoard: "Authentication", identifier: "AuthVC")
    }
}
