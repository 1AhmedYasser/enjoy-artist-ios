//
//  ViewController.swift
//  quickstart-ios-swift
//
//  Created by Lara Vertlberg on 09/12/2019.
//  Copyright © 2019 Lara Vertlberg. All rights reserved.
//

import UIKit
import DeepAR
import RecordButton

enum Mode: String {
    case masks
    case effects
    case filters
}

enum Masks: String, CaseIterable {
    case none
    case aviators
    case bigmouth
    case dalmatian
    case fatify
    case flowers
    case grumpycat
    case kanye
    case koala
    case lion
    case mudMask
    case obama
    case pug
    case slash
    case sleepingmask
    case smallface
    case teddycigar
    case tripleface
    case twistedFace
}

enum Effects: String, CaseIterable {
    case none
    case fire
    case heart
    case blizzard
    case rain
}

enum Filters: String, CaseIterable {
    case none
    case tv80
    case drawingmanga
    case sepia
    case bleachbypass
    case realvhs
    case filmcolorperfection
}

class ARViewController: UIViewController {
    
    // MARK: - IBOutlets -
    
    @IBOutlet weak var switchCameraButton: UIButton!
    
    @IBOutlet weak var masksButton: UIButton!
    @IBOutlet weak var effectsButton: UIButton!
    @IBOutlet weak var filtersButton: UIButton!
    
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var takeScreenshotButton: UIButton!
    
    @IBOutlet weak var arView: ARView!
    
    var delegate: ableToReceiveData?
    var videoPath:String?
    
    var isRecording = false
    
    // MARK: - Private properties -
    
    private var maskIndex: Int = 0
    private var maskPaths: [String?] {
        return Masks.allCases.map { $0.rawValue.path }
    }
    
    private var effectIndex: Int = 0
    private var effectPaths: [String?] {
        return Effects.allCases.map { $0.rawValue.path }
    }
    
    private var filterIndex: Int = 0
    private var filterPaths: [String?] {
        return Filters.allCases.map { $0.rawValue.path }
    }
    
    private var buttonModePairs: [(UIButton, Mode)] = []
    private var currentMode: Mode! {
        didSet {
            updateModeAppearance()
        }
    }
    
    @IBOutlet weak var recordButton: RecordButton!
    var progressTimer : Timer!
    var startRecordingTimer: Timer!
    var progress : CGFloat! = 0
    var startedRecording = false
    // MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTargets()
        buttonModePairs = [(masksButton, .masks), (effectsButton, .effects), (filtersButton, .filters)]
        
        currentMode = .masks
        recordButton.addTarget(self, action: #selector(ARViewController.record), for: .touchDown)
        recordButton.addTarget(self, action: #selector(ARViewController.stop), for: UIControl.Event.touchUpInside)
    }
    
    @objc func record() {
        self.startRecordingTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(ARViewController.startRecording), userInfo: nil, repeats: false)
        self.progressTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(ARViewController.updateProgress), userInfo: nil, repeats: true)
    }
    
    @objc func startRecording() {
        print("recording")
        arView.startRecording()
    }
    
    @objc func updateProgress() {
        
        let maxDuration = CGFloat(5) // Max duration of the recordButton
        
        progress = progress + (CGFloat(0.05) / maxDuration)
        recordButton.setProgress(progress)
        
        if progress >= 1 {
            progress = 0
            print("reached max")
        }
        startedRecording = true
    }
    
    @objc func stop() {
        if startedRecording == true {
          //  print("take a screen shot")
            //arView.takeScreenshot()
            print("Save Video")
            arView.finishRecording()
        }
        print("Stopped")
        progress = 0
        startedRecording = false
        self.progressTimer.invalidate()
        self.startRecordingTimer.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupArView()
        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if videoPath != nil {
            delegate?.passedData(data: self.videoPath ?? "")
        }
        arView.shutdown()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        // called to stop the camera and prepare for changing the camera orientation
        arView.changeOrientationStart()
        // sometimes UIDeviceOrientationDidChangeNotification will be delayed, so we call orientationChanged in 0.5 seconds anyway
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
            self?.orientationDidChange()
        }
    }
    
    // MARK: - Private methods -
    
    private func setupArView() {
        arView.setLicenseKey("613bda5370b7e0705cdd4eaca142ac65eac8ed5cf87534277bc470504705fbef9129fcf293cd3af4")
        arView.delegate = self
        arView.initialize()
    }
    
    @IBAction func closeAR(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func addTargets() {
        switchCameraButton.addTarget(self, action: #selector(didTapSwitchCameraButton), for: .touchUpInside)
        //takeScreenshotButton.addTarget(self, action: #selector(didTapTakeScreenshotButton), for: .touchUpInside)
        previousButton.addTarget(self, action: #selector(didTapPreviousButton), for: .touchUpInside)
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
        masksButton.addTarget(self, action: #selector(didTapMasksButton), for: .touchUpInside)
        effectsButton.addTarget(self, action: #selector(didTapEffectsButton), for: .touchUpInside)
        filtersButton.addTarget(self, action: #selector(didTapFiltersButton), for: .touchUpInside)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(didTapPreviousButton))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)

        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(didTapNextButton))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    private func updateModeAppearance() {
        buttonModePairs.forEach { (button, mode) in
            button.isSelected = mode == currentMode
        }
    }
    
    private func switchMode(_ path: String?) {
        arView.switchEffect(withSlot: currentMode.rawValue, path: path)
    }
    
    @objc
    private func orientationDidChange() {
        if #available(iOS 13.0, *) {
            guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else { return }
            arView.change(orientation)
        } else {
            // Fallback on earlier versions
        }
        // called to reinitialize the engine with the new camera and rendering resolution
    }
    
    @objc
    private func didTapSwitchCameraButton() {
        let position: AVCaptureDevice.Position = arView.getCameraPosition() == .back ? .front : .back
        arView.switchCamera(position)
    }
    
    @objc
    private func didTapTakeScreenshotButton() {
        //arView.takeScreenshot()
        if isRecording {
            arView.finishRecording()
            isRecording = false
        } else {
            arView.startRecording()
            isRecording = true
        }
    }
    
    @objc
    private func didTapPreviousButton() {
        var path: String?
        
        switch currentMode! {
        case .effects:
            effectIndex = (effectIndex - 1 < 0) ? (effectPaths.count - 1) : (effectIndex - 1)
            path = effectPaths[effectIndex]
        case .masks:
            maskIndex = (maskIndex - 1 < 0) ? (maskPaths.count - 1) : (maskIndex - 1)
            path = maskPaths[maskIndex]
        case .filters:
            filterIndex = (filterIndex - 1 < 0) ? (filterPaths.count - 1) : (filterIndex - 1)
            path = filterPaths[filterIndex]
        }
        
        switchMode(path)
    }
    
    @objc
    private func didTapNextButton() {
        var path: String?
        
        switch currentMode! {
        case .effects:
            effectIndex = (effectIndex + 1 > effectPaths.count - 1) ? 0 : (effectIndex + 1)
            path = effectPaths[effectIndex]
        case .masks:
            maskIndex = (maskIndex + 1 > maskPaths.count - 1) ? 0 : (maskIndex + 1)
            path = maskPaths[maskIndex]
        case .filters:
            filterIndex = (filterIndex + 1 > filterPaths.count - 1) ? 0 : (filterIndex + 1)
            path = filterPaths[filterIndex]
        }
        
        switchMode(path)
    }
    
    @objc
    private func didTapMasksButton() {
        currentMode = .masks
    }
    
    @objc
    private func didTapEffectsButton() {
        currentMode = .effects
    }
    
    @objc
    private func didTapFiltersButton() {
        currentMode = .filters
    }
}

// MARK: - ARViewDelegate -

extension ARViewController: ARViewDelegate {
    func didFinishPreparingForVideoRecording() {print("Prepared For recording")}
    
    func didStartVideoRecording() {print("Started Recording")}
    
    func didFinishVideoRecording(_ videoFilePath: String!) {
        
         //        //uncomment this if you want to save the video file to the media library
//                        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoFilePath) {
//                            UISaveVideoAtPathToSavedPhotosAlbum(videoFilePath, self, nil, nil)
//                        }
        
        videoPath = videoFilePath
        self.dismiss(animated: true, completion: nil)
        print("Finished Recording")
        
        
    }
    
    func recordingFailedWithError(_ error: Error!) {print("Recording Failed")}
    
    func didTakeScreenshot(_ screenshot: UIImage!) {
        UIImageWriteToSavedPhotosAlbum(screenshot, nil, nil, nil)
        
        let imageView = UIImageView(image: screenshot)
        imageView.frame = view.frame
        view.insertSubview(imageView, aboveSubview: arView)
        
        let flashView = UIView(frame: view.frame)
        flashView.alpha = 0
        flashView.backgroundColor = .black
        view.insertSubview(flashView, aboveSubview: imageView)
        
        UIView.animate(withDuration: 0.1, animations: {
            flashView.alpha = 1
        }) { _ in
            flashView.removeFromSuperview()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                imageView.removeFromSuperview()
            }
        }
    }
    
    func didInitialize() {}
    
    func faceVisiblityDidChange(_ faceVisible: Bool) {}
}

extension String {
    var path: String? {
        return Bundle.main.path(forResource: self, ofType: nil)
    }
}
