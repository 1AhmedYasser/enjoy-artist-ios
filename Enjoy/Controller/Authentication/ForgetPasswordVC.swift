//
//  ForgetPasswordVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/11/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class ForgetPasswordVC: UIViewController {
    
    @IBOutlet weak var email: DesignableUITextField!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            backButton.transform = CGAffineTransform(scaleX: -1, y: 1)
        }
    }
    
    @IBAction func sendEmail(_ sender: UIButton) {
        if email.text!.isEmpty {
            self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: "Please Enter Your Email Address".localized,vc: self){}
            return
        }
        
        if isValidEmail(email) == false {
            self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: "Your Email Address is not valid".localized,vc: self){}
            return
        }
        
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        let parameters = ["email":email.text!]
        EnjoyAPI.EnjoyRequest(NConstants.forgetPassword,EnjoyError.self,parameters,isHeaders: false,.post) { (message, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            sender.isEnabled = true
            if error != nil {
                self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: error!.localizedDescription,vc: self){}
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: errorMessage,vc: self){}
            } else {
                if message!.success == true {
                    self.enjoyAlert(image: #imageLiteral(resourceName: "OrderAccepted"), message: message!.message,vc: self) {
                    self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: message!.message,vc: self){}
                }
            }
        }
        
    }
}
