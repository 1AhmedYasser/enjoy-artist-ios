//
//  LoginVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoginVC: UIViewController {
    
    @IBOutlet weak var email: DesignableUITextField!
    @IBOutlet weak var password: DesignableUITextField!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    
    var rememberMe: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    @IBAction func rememberMe(_ sender: UIButton) {
        rememberMe = !rememberMe
        (rememberMe) ? sender.setImage(#imageLiteral(resourceName: "Tick"), for: .normal) : sender.setImage(#imageLiteral(resourceName: "EmptyTick"), for: .normal)
    }
    
    @IBAction func login(_ sender: DesignableButton) {
        if email.text!.isEmpty {
            self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: "Please Enter Your Email Address".localized,vc: self){}
            return
        }
        
        if password.text!.isEmpty {
            self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: "Please Enter Your Password".localized,vc: self){}
            return
        }
        
        if isValidEmail(email) == false {
            self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: "Your Email Address is not valid".localized,vc: self){}
            return
        }
        
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        let parameters = ["email":email.text!,"password":password.text!,"firebase_token":EnjoyData.shared.getStringValue(Constants.fcmToken)]
        EnjoyAPI.EnjoyRequest(NConstants.login,User.self,parameters,isHeaders: false,.post) { (user, errorMessage, error) in
            sender.isEnabled = true
            if error != nil {
                self.loadingIndicator.stopAnimating()
                self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: error!.localizedDescription,vc: self){}
                return
            }
            
            if errorMessage.isEmpty == false {
                self.loadingIndicator.stopAnimating()
                self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: errorMessage,vc: self){}
            } else {
                (self.rememberMe == true) ? self.save(true,Constants.userLogged) : nil
                EnjoyData.shared.SaveUserInfo(user: user?.data)
                if user!.success == true {
                    EnjoyAPI.EnjoyRequest(NConstants.notificationsCount,NotificationsCount.self) { (count, errorMessage, error) in
                        self.loadingIndicator.stopAnimating()
                        if error != nil {
                            print(error!.localizedDescription)
                            return
                        }
                        
                        if errorMessage.isEmpty == false {
                            print(errorMessage)
                        } else {
                            self.save(count?.data, Constants.notificationCount)
                            self.openVC(storyBoard: "Authentication", identifier: "HomeMainVC")
                        }
                    }
                } else {
                    self.enjoyAlert(image: #imageLiteral(resourceName: "OrderRejected"), message: user!.message,vc: self){}
                }
            }
        }
    }
}
