//
//  MainVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/14/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH
import AKSideMenu

public class MainVC: AKSideMenu, AKSideMenuDelegate {
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        self.menuPreferredStatusBarStyle = .lightContent
        self.contentViewShadowColor = .black
        self.contentViewShadowOffset = CGSize(width: 0, height: 0)
        self.contentViewShadowOpacity = 0.6
        self.contentViewShadowRadius = 12
        self.contentViewShadowEnabled = true
        self.backgroundImage = UIImage(named: "HomeBackground")
        
        var vc: UIViewController?
        if EnjoyData.shared.getStringValue(Constants.userToken).isEmpty {
            vc = self.storyboard!.instantiateViewController(withIdentifier: "AuthVC")
        } else {
            vc = UIStoryboard(name: "Menu", bundle: nil).instantiateViewController(withIdentifier: "ArtistProfileVC")
            
            EnjoyAPI.EnjoyRequest(NConstants.notificationsCount,NotificationsCount.self) { (count, errorMessage, error) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    print(errorMessage)
                } else {
                    self.save(count?.data, Constants.notificationCount)
                }
            }
        }
        
        let contentVC = UINavigationController(rootViewController: vc!)
        contentVC.navigationBar.barStyle = .black
        applyEnjoyGradient(nav: contentVC,isTransparent: true)
        self.contentViewController = contentVC
        
        if MOLHLanguage.currentAppleLanguage() == "en" {
            self.leftMenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuVC")
        } else {
            self.rightMenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuVC")
        }
    }
}

