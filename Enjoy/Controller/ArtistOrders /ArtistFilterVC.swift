//
//  ArtsitFilterVC.swift
//  Enjoy
//
//  Created by IMac on 12/29/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import iOSDropDown
import MOLH
import NVActivityIndicatorView


class ArtistFilterVC: UIViewController {
    
    
    @IBOutlet weak var categoryMenu: DropDown!
    @IBOutlet weak var keyTextField: DesignableUITextField!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var dateFrom: DesignableUITextField!
    @IBOutlet weak var dateTo: DesignableUITextField!
    @IBOutlet weak var orderStatus: UISegmentedControl!
    
    
    var datePicker = UIDatePicker()
    var dateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    
    var orders = [OrderStateData]()
    
    var selectedOrder = [OrderStateData]()
    
    var categories = [Category]() {
        didSet {
            for category in categories {
                categoryMenu.optionArray.append(category.name)
            }
        }
    }
    
    var selectedCategory: Category?
    var orderStatusStr : String = ""
    var dateFromValue : Date?
    var dateToValue : Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyEnjoyGradient(nav: self.navigationController!)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            dateFrom.textAlignment = .right
            dateTo.textAlignment = .right
            keyTextField.textAlignment = .right
            categoryMenu.textAlignment = .right
        } else {
            dateFrom.textAlignment = .left
            dateTo.textAlignment = .left
            keyTextField.textAlignment = .left
            categoryMenu.textAlignment = .left
        }
        
        
        dateFrom.inputView = datePicker
        dateTo.inputView = datePicker
        
        
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped))
        toolBar.setItems([doneButton], animated: true)
        dateFrom.inputAccessoryView   = toolBar
        dateTo.inputAccessoryView     = toolBar
        
        if #available(iOS 13.0, *) {
            
            orderStatus.setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
            
        } else {
            orderStatus.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .normal)
        }
        loadCategories()
        
        categoryMenu.selectedRowColor = UIColor.lightGray
        
        categoryMenu.didSelect{(selectedText , index ,id) in
            self.selectedCategory = self.categories[index]
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)
           if MOLHLanguage.currentAppleLanguage() == "ar" {
               let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
               backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
               self.navigationItem.leftBarButtonItem  = backButton
           }
       }
    
    fileprivate func loadCategories() {
        EnjoyAPI.EnjoyRequest(NConstants.categories,clients: false, Categories.self,isHeaders: false) { (categories, errorMessage, error) in
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.loadingIndicator.stopAnimating()
                self.enjoyToast(errorMessage)
            } else {
                if categories!.success == true {
                    self.categories = categories!.data
                } else {
                    self.enjoyToast(categories!.message)
                }
            }
        }
    }
    
    
    @IBAction func resetBtn(_ sender: UIBarButtonItem) {
        
        orderStatus.selectedSegmentIndex = 0
        keyTextField.text = ""
        dateFrom.text = ""
        dateTo.text = ""
        categoryMenu.text = ""
    }
    
    @IBAction func applyBtn(_ sender: DesignableButton) {
        
        let categoryName : String = selectedCategory?.name ?? ""
        
        if orderStatus.selectedSegmentIndex == 0
        {
            self.orderStatusStr = "Pending"
        }
        else if orderStatus.selectedSegmentIndex == 1
        {
            self.orderStatusStr = "Ready"
        }
        else if orderStatus.selectedSegmentIndex == 2
        {
            self.orderStatusStr = "Done"
        }
        else if orderStatus.selectedSegmentIndex == 3
        {
            self.orderStatusStr = "Completed"
        }
        selectedOrder = orders.filter { (orders : OrderStateData ) -> Bool in
            
            //    let createdDate = dateFormatter.date(from: orders.created_at)
            
            
            return orders.category_title.contains(categoryName) || orders.artist_status.contains(orderStatusStr)
        }
        
        
        
        print(selectedOrder)
        
        let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "FiltersData") as! FiltersData
        vc.selectedState = self.selectedOrder
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func datesRange(from: Date, to: Date) -> [Date] {
        
        if from > to { return [Date]() }
        
        var tempDate = from
        var array = [tempDate]
        
        while tempDate < to {
            tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
            array.append(tempDate)
        }
        
        return array
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateFrom {
            datePicker.datePickerMode = .date
        }
        if textField == dateTo {
            datePicker.datePickerMode = .date
        }
    }
    
    @objc func doneButtonTapped() {
        if dateFrom.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateFrom.text = dateFormatter.string(from: datePicker.date)
            self.dateFromValue = datePicker.date
        }
        if dateTo.isFirstResponder {
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .none
            dateFormatter.dateFormat = "yyyy-MM-dd"
            dateTo.text = dateFormatter.string(from: datePicker.date)
            self.dateToValue = datePicker.date
            
        }
        
        self.view.endEditing(true)
    }
    
}
extension UIButton {
    func setBackgroundColor(_ color: UIColor, forState controlState: UIControl.State) {
        let colorImage = UIGraphicsImageRenderer(size: CGSize(width: 1, height: 1)).image { _ in
            color.setFill()
            UIBezierPath(rect: CGRect(x: 0, y: 0, width: 1, height: 1)).fill()
        }
        setBackgroundImage(colorImage, for: controlState)
    }
}

extension Date {
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate
        
        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}
