//
//  CompleteOrderVC.swift
//  Enjoy
//
//  Created by IMac on 1/1/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class CompleteOrderVC: UIViewController {
    
    @IBOutlet weak var indicatorActivity: NVActivityIndicatorView!
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var serviceImg: UIImageView!
    @IBOutlet weak var serviceDesc: UITextView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var orders : EstimateOrderData?
    var order: OrderStateData?
    var orderStatus : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //LoadOrder()
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        self.serviceDesc.text = order?.video.description
        self.serviceType.text = order?.category_title
        self.serviceImg.kf.setImage(with: URL(string: order?.category_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    @IBAction func compeleteOrder(_ sender: DesignableButton) {
        
        indicatorActivity.startAnimating()
        sender.isEnabled = false
        
        let parameters = ["order_id" : order!.id]
        EnjoyAPI.EnjoyRequest(NConstants.completeOrder,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
            self.indicatorActivity.stopAnimating()
            sender.isEnabled = true
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if message!.success == true {
                    self.enjoyToast(errorMessage)
                    self.goBack(sender)
                } else {
                    self.enjoyToast(message!.message)
                }
            }
        }
    }
    
}
