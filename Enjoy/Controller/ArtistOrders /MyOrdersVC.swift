//
//  MyOrdersVC.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import EmptyDataSet_Swift
import MOLH

class MyOrdersVC: UIViewController {
    
    @IBOutlet weak var statesCV: UICollectionView!
    @IBOutlet weak var OrderStatesCV: UICollectionView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var statesFlowlayout: UICollectionViewFlowLayout!

    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    let states: [String] = ["All","New Request","Ready","Pending","Completed","Done","Rejected","Canceled"]
    var orders = [OrderStateData]()
    
    var order_id: Int?
    
    var currentState: String = "All"
    
    var selectedState = [OrderStateData]() {
        didSet {
            UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.OrderStatesCV.reloadData()
            }, completion: nil)
        }
    }
    
    var stateSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyEnjoyGradient(nav: self.navigationController!,collectionView: statesCV)
        statesCV.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
        
        statesFlowlayout.itemSize = UICollectionViewFlowLayout.automaticSize
        statesFlowlayout.estimatedItemSize = CGSize(width: 76, height: 50)
        
        //LoadOrders()
        
    }
    
    @IBAction func openFilter(_ sender: UIBarButtonItem) {
        
        let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "ArtistFilterVC") as! ArtistFilterVC
        vc.orders = self.orders
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        LoadOrders()
    }
    
    func showEmptyResultMessage(show: Bool,status: String) {
        if show {
            OrderStatesCV.emptyDataSetView { (view) in
                view.image(UIImage(named: "NoOrders")).detailLabelString(NSAttributedString(string: "No Orders".localized)).shouldFadeIn(true).verticalSpace(37.2)
            }
        } else {
            OrderStatesCV.emptyDataSetView { (view) in
                view.detailLabelString(NSAttributedString(string: ""))
                
            }
        }
    }
    
    fileprivate func LoadOrders() {
        loadingIndicator.startAnimating()
        EnjoyAPI.EnjoyRequest(NConstants.orders,OrderState.self,isHeaders: true) { (orders, errorMessage, error) in
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
               // self.enjoyToast(errorMessage)
            } else {
                if orders!.success == true {
                    self.orders = orders!.data
                    self.statesCV.isUserInteractionEnabled = true
                    UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        self.OrderStatesCV.reloadData()
                    }, completion: nil)
                } else {
                  // self.enjoyToast(orders!.message)
                }
            }
        }
    }
    
}


extension MyOrdersVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == statesCV {
            return states.count
        } else {
            let rows = (stateSelected) ? selectedState.count : orders.count
            if rows == 0 {
                (currentState == "All") ? showEmptyResultMessage(show: true,status: "") : showEmptyResultMessage(show: true,status: currentState)
            } else {
                showEmptyResultMessage(show: false,status: "")
            }
            return rows
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == statesCV {
            let orderStateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderStateCell", for: indexPath) as! OrderStateCell
            orderStateCell.stateName.text = states[indexPath.row].localized
            return orderStateCell
        } else {
            let stateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StateCell", for: indexPath) as! StateCell
            stateCell.setupCell(order: (stateSelected) ? selectedState[indexPath.row] : orders[indexPath.row])
            return stateCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == statesCV {
            if indexPath.row != 0 {
                stateSelected = true
                currentState = states[indexPath.row]
                selectedState = orders.filter {$0.artist_status == states[indexPath.row]}
            } else {
                stateSelected = false
                currentState = ""
                UIView.transition(with: self.OrderStatesCV, duration: 0.5, options: .transitionCrossDissolve, animations: {
                    self.OrderStatesCV.reloadData()
                }, completion: nil)
            }
        } else {
            
            self.order_id = orders[indexPath.row].id
            let state = (stateSelected) ? selectedState[indexPath.row] : orders[indexPath.row]
            switch state.artist_status {
            case "New Request":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "EstimationOrderVC") as! EstimationOrderVC
            vc.order = state
            self.navigationController?.pushViewController(vc, animated: true)
            case "Completed":
            openVC(storyBoard: "ArtistOrders", identifier: "DoneOrderVC")
            case "Done":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "CompleteOrderVC") as! CompleteOrderVC
            vc.order = state
            self.navigationController?.pushViewController(vc, animated: true)
            case "Pending":
            openVC(storyBoard: "ArtistOrders", identifier: "PendingOrderVC")
            case "Ready":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "ReadyOrderVC") as! ReadyOrderVC
            vc.orderID = self.order_id!
            self.navigationController?.pushViewController(vc, animated: true)
            default:
            print("do nothing")
            }
        }
    }
    
    @IBAction func backToOrders(_ sender: UIStoryboardSegue) {}
}

