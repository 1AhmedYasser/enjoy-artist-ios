//
//  EstimationOrderVC.swift
//  Enjoy
//
//  Created by IMac on 1/1/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Kingfisher
import MOLH

class EstimationOrderVC: UIViewController {
    
    
    @IBOutlet weak var serviceType: DesignableLabel!
    @IBOutlet weak var serviceDesc: UITextView!
    @IBOutlet weak var indicatorActivity: NVActivityIndicatorView!
    @IBOutlet weak var serviceImg: UIImageView!
    @IBOutlet weak var dateTxtField: DesignableUITextField!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var orders : EstimateOrderData?
    var order: OrderStateData?
    var orderStatus : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            dateTxtField.textAlignment = .right
        } else {
            dateTxtField.textAlignment = .left
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EstimationOrderVC.tapReconizer(tapRec:)))
        view.addGestureRecognizer(tapGesture)
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.maximumDate = Date()
        let components: NSDateComponents = NSDateComponents()
        components.year = -100
        datePickerView.minimumDate = datePickerView.calendar.date(byAdding: components as DateComponents, to: Date())
        dateTxtField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(dataPickerValueChanged(sender : )), for: UIControl.Event.valueChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        serviceType.text = order?.category_title
        serviceDesc.text = order?.video.description
        serviceImg.kf.setImage(with: URL(string: order?.category_image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    @IBAction func sendBtn(_ sender: DesignableButton) {
        proccessOrder(sender: sender, status: "Accepted")
    }
    
    
    @IBAction func cancelBtn(_ sender: DesignableButton) {
        proccessOrder(sender: sender, status: "Rejected")
    }
    
    func proccessOrder(sender:UIButton , status: String) {
        if self.dateTxtField.text!.isEmpty && status == "Accepted" {
            self.enjoyToast("Please Choose an estimation Date".localized)
        } else {
            indicatorActivity.startAnimating()
            sender.isEnabled = false
            
            let parameters = ["order_id" : order!.id , "order_status" : status , "estimation_time" : self.dateTxtField.text!] as [String : Any]
            EnjoyAPI.EnjoyRequest(NConstants.estimateOrder,EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
                self.indicatorActivity.stopAnimating()
                sender.isEnabled = true
                if error != nil {
                    self.enjoyToast(error!.localizedDescription)
                    return
                }
                
                if errorMessage.isEmpty == false {
                    self.enjoyToast(errorMessage)
                } else {
                    if message!.success == true {
                        self.enjoyToast(errorMessage)
                        self.goBack(sender)
                    } else {
                        self.enjoyToast(message!.message)
                    }
                }
            }
        }
        
    }
    
    @objc func dataPickerValueChanged(sender : UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let selectedDate = dateFormatter.string(from: sender.date)
        dateTxtField.text = "\(selectedDate)"
    }
    
    @objc func tapReconizer(tapRec : UITapGestureRecognizer)  {
        view.endEditing(true)
    }
}
