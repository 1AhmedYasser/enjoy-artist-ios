//
//  PendingOrderVC.swift
//  Enjoy
//
//  Created by IMac on 1/1/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH

class PendingOrderVC: UIViewController {
    
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
}
