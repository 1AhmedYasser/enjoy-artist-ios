//
//  FiltersData.swift
//  Enjoy
//
//  Created by IMac on 1/8/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import MOLH

class FiltersData: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var OrderStatesCV: UICollectionView!
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    
    var order_id : Int?
    var selectedState = [OrderStateData]()
    var stateSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        OrderStatesCV.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedState.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let stateCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StateCell", for: indexPath) as! StateCell
        stateCell.setupCell(order: selectedState[indexPath.row] )
        return stateCell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.order_id = selectedState[indexPath.row].id
        let state = selectedState[indexPath.row].artist_status
        
        switch state {
        case "New Request":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "EstimationOrderVC") as! EstimationOrderVC
            vc.order = selectedState[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        case "Completed":
            openVC(storyBoard: "ArtistOrders", identifier: "DoneOrderVC")
        case "Done":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "CompleteOrderVC") as! CompleteOrderVC
            vc.order = selectedState[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        case "Pending":
            openVC(storyBoard: "ArtistOrders", identifier: "PendingOrderVC")
        case "Ready":
            let vc = UIStoryboard(name: "ArtistOrders", bundle: nil).instantiateViewController(withIdentifier: "ReadyOrderVC") as! ReadyOrderVC
            vc.orderID = self.order_id!
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("do nothing")
        }
    }
}
