//
//  ReadyOrderVC.swift
//  Enjoy
//
//  Created by IMac on 1/1/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import MOLH
import NVActivityIndicatorView

class ReadyOrderVC: UIViewController, ableToReceiveData {
    
    func passedData(data: String) {
        if data.isEmpty == false {
            print("Video Path is \(data)")
            addVideoStack.isHidden = true
            videoView.isHidden = false
            //addVideoStack.isHidden = true
            videoURL = URL(fileURLWithPath: data)
            thumbnail = generateThumbnail(url: URL(fileURLWithPath: data))
            //  load Video
            player = AVPlayer(url: URL(fileURLWithPath: data))
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.controlsView.frame
            videoView.layer.masksToBounds = true
            playerLayer.videoGravity = AVLayerVideoGravity.resize
            self.videoView.layer.insertSublayer(playerLayer, at: 0)
        } else {
            print("Empty Video Path")
        }
    }
    
    
    @IBOutlet weak var notificationIcon: BadgeBarButtonItem!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var loadingIndicator: NVActivityIndicatorView!
    @IBOutlet weak var controlsView: UIView!
    @IBOutlet weak var addVideoImage: UIImageView!
    @IBOutlet weak var addVideoStack: UIStackView!
    @IBOutlet weak var statusButton: UIButton!
    
    var pickerController =  UIImagePickerController()
    var player: AVPlayer?
    var videoURL:URL?
    var thumbnail: String?
    var orderID : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyEnjoyGradient(nav: self.navigationController!)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(addVideo))
        addVideoImage.isUserInteractionEnabled = true
        addVideoImage.addGestureRecognizer(tapGestureRecognizer)
        
        
        self.pickerController.delegate = self
        self.pickerController.allowsEditing = true
        self.pickerController.mediaTypes = ["public.movie"]
        self.pickerController.videoQuality = .typeHigh
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notificationIcon.badgeNumber = EnjoyData.shared.getIntValue(Constants.notificationCount)
        
        if MOLHLanguage.currentAppleLanguage() == "ar" {
            let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "RightArrowLanguage"), style: .plain, target: self, action: #selector(goBack(_:)))
            backButton.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem  = backButton
        }
        
    }
    
    
    private func pickerController(_ controller: UIImagePickerController, didSelect url: URL?) {
        if let url = url {
            videoView.isHidden = false
            addVideoStack.isHidden = true
            videoURL = url
            thumbnail = generateThumbnail(url: url)
            //  load Video
            player = AVPlayer(url: url)
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.controlsView.frame
            videoView.layer.masksToBounds = true
            playerLayer.videoGravity = AVLayerVideoGravity.resize
            self.videoView.layer.insertSublayer(playerLayer, at: 0)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        controller.dismiss(animated: true, completion: nil)
    }
    
    @objc func playerDidFinishPlaying() {
        statusButton.setImage(#imageLiteral(resourceName: "SliderReplay"), for: .normal)
    }
    
    private func action(for type: UIImagePickerController.SourceType, title: String) -> UIAlertAction? {
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            return nil
        }
        
        return UIAlertAction(title: title, style: .default) { [unowned self] _ in
            self.pickerController.sourceType = type
            self.present(self.pickerController, animated: true)
        }
    }
    
    @IBAction func addVideo(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //           if let action = self.action(for: .camera, title: "Take video") {
        //               alertController.addAction(action)
        //           }
        
        alertController.addAction(UIAlertAction(title: "Take Video", style: .default) { (action) in
            let arViewController = UIStoryboard(name: "Common", bundle: nil).instantiateViewController(withIdentifier: "ARViewController") as! ARViewController
            arViewController.delegate = self
            arViewController.modalPresentationStyle = .overCurrentContext
            self.present(arViewController, animated: true, completion: nil)
        })
        if let action = self.action(for: .savedPhotosAlbum, title: "Camera roll") {
            alertController.addAction(action)
        }
        if let action = self.action(for: .photoLibrary, title: "Video library") {
            alertController.addAction(action)
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertController, animated: true)
    }
    
    @IBAction func handleVideoState(_ sender: UIButton) {
        if let player = self.player {
            if sender.imageView?.image == UIImage(named: "SliderReplay") {
                sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "playSlider"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                    player.play()
                }
            }
        }
    }
    
    
    
    @IBAction func submitVideo(_ sender: UIButton) {
        if player == nil {
            enjoyToast("Please choose a video".localized)
            return
        }
        loadingIndicator.startAnimating()
        sender.isEnabled = false
        let parameters = ["video": convertVideoToBase64(videoURL!),
                          "order_id" : self.orderID] as [String : Any]
        EnjoyAPI.EnjoyRequest(NConstants.uploadOrderVideo, EnjoyError.self,parameters,isHeaders: true,.post) { (message, errorMessage, error) in
            sender.isEnabled = true
            self.loadingIndicator.stopAnimating()
            if error != nil {
                self.enjoyToast(error!.localizedDescription)
                return
            }
            
            if errorMessage.isEmpty == false {
                self.enjoyToast(errorMessage)
            } else {
                if message!.success == true {
                    print(message!)
                    self.navigationController?.popViewController(animated: true)
                    self.enjoyToast(message!.message)
                } else {
                    self.enjoyToast(message!.message)
                }
            }
        }
    }
    
    func generateThumbnail(url: URL) -> String {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        let time = CMTimeMake(value: 1, timescale: 1)
        let imageRef = try! imageGenerator.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage:imageRef)
        return convertImageToBase64(thumbnail)
    }
}
extension ReadyOrderVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.pickerController(picker, didSelect: nil)
    }
    
    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        guard let url = info[.mediaURL] as? URL else {
            return self.pickerController(picker, didSelect: nil)
        }
        
        //        //uncomment this if you want to save the video file to the media library
        //        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path) {
        //            UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, nil, nil)
        //        }
        self.pickerController(picker, didSelect: url)
    }
}
