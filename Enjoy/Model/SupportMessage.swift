//
//  SupportMessage.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/24/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct Message {
    let CreatedTime: Any
    let Message: String
    let id: String
    let sender: String
    
    init(data: [String: Any]) {
        self.CreatedTime = data["CreatedTime"] as Any
        self.Message = data["Message"] as? String ?? ""
        self.id = data["id"] as? String ?? ""
        self.sender = data["sender"] as? String ?? ""
    }
}

struct GuestMessage {
    let CreatedTime: Any
    let Message: String
    let Name: String
    let id: String
    let sender: String
    
    init(data: [String: Any]) {
        self.CreatedTime = data["CreatedTime"] as Any
        self.Message = data["Message"] as? String ?? ""
        self.Name = data["Name"] as? String ?? ""
        self.id = data["id"] as? String ?? ""
        self.sender = data["sender"] as? String ?? ""
    }
}
