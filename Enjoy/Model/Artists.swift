//
//  Artists.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/16/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct Artists: Codable {
    let status_code: Int
    let success: Bool
    let data: [ArtistsData]
    let message: String
}

struct ArtistsData: Codable {
    let id, price: Int
    let promo_video: String
    let image: String
    let name: String
    let orders_no, videos_no, payments: Int
    let profile_videos: [ProfileVideo]
}

struct ProfileVideo: Codable {
    let id: Int
    let title,title_en, title_ar, description_en, description_ar: String?
    let views, likes: Int
    let category_id, artist_id : Int?
    let featured: String
    let thumb_nail: String?
    let profile_video: String
    let video: String
    let created_at, updated_at: String?
    let profile_video_description: String?
    let artist: Artist
    let category: Category
}

struct Artist: Codable {
    let id: Int
    let image: String?
    let price: Int
    let name: String
    let firebase_token: String?
    
}

struct Categories: Codable {
    let status_code: Int
    let success: Bool
    let data: [Category]
    let message: String
}

struct Category: Codable {
    let id: Int
    let name: String
}

struct PromoVideo: Codable {
    let status_code: Int
    let success: Bool
    let data: PromoData
    let message: String
}

struct PromoData: Codable {
    let promo_video: String?
}

struct ArtistInfo: Codable {
    let status_code: Int
    let success: Bool
    let data: ArtistInfoData
    let message: String
}

struct ArtistInfoData: Codable {
    let profile_videos: [ProfileVideo]
}
