//
//  Order.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/30/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct OrderState: Codable {
    let status_code: Int
    let success: Bool
    let data: [OrderStateData]
    let message: String
}

struct OrderStateData: Codable {
   let id: Int
   let created_at, artist_status: String
   let video_id: Int
   let category_image: String
   let category_title: String
   let video: Video
}

struct Video: Codable {
    let id: Int
    let title: String
    let description_en: String
    let category_id, artist_id, views, likes: Int
    let featured: String
    let profile_video: String
    let video: String?
    let created_at, updated_at: String
    let liked: Bool
    let title_en, description: String?
    let artist: ArtistClass
    let category: Category
}

struct ArtistClass: Codable {
    let id: Int
    let image: String
    let price: Int
    let firebase_token, name: String
}
