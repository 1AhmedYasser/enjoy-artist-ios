//
//  ContactUs.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct ContactUs: Codable {
    let status_code: Int
    let success: Bool
    let data: ContactUsData
    let message: String
}

struct ContactUsData: Codable {
    let phone_number, contact_email: String
    let facebook_link, instgram_link, snapchat_link, twitter_link,youtube_link: String
}
