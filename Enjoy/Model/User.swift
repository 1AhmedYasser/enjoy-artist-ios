//
//  User.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct User: Codable {
    let status_code: Int
    let success: Bool
    let data: UserData
    let message: String
}

struct UserData: Codable {
    
    let id: Int
    let name_en: String
    let name_ar: String
    let email: String
    let image: String
    let price: Int
    let promo_video: String
    let valid: String
    let name: String
    let token: String
    let orders_no: Int
    let videos_no: Int
    let payments: Int
    let firebase_token: String?
    let profile_videos: [ProfileVideo]
    
}

