//
//  Estimation .swift
//  Enjoy
//
//  Created by IMac on 1/2/20.
//  Copyright © 2020 Ahmed yasser. All rights reserved.
//

import Foundation

struct EstimateOrder: Codable {
    let status_code: Int?
    let success: Bool?
    let data: EstimateOrderData
    let message: String?
}

struct EstimateOrderData: Codable {
   
    let id: Int?
    let created_at: String?
    let artist_status: String?
    let reason_en: String?
    let reason_ar: String?
    let video_title: String?
    let video_description: String?
    let category_image: String?
    let category_title: String?
    let artist_title: String?
    
}
