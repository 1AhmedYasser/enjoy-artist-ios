//
//  EnjoyError.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/10/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import Foundation

struct EnjoyError: Codable {
    let status_code: Int
    let success: Bool?
    let message: String
}
