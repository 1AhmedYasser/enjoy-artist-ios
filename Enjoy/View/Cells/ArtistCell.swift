//
//  ArtistCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/16/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import MOLH
import Kingfisher

class ArtistCell: UITableViewCell {
    
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistImage: DesignableImageView!
    @IBOutlet weak var worksOfArts: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selected ? (contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)) : (contentView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
    }
    
    func setupCell (artists : ArtistsData) {
        artistImage!.kf.setImage(with: URL(string: artists.image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? artists.image),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        artistName.text = artists.name
        worksOfArts.text = "\(artists.orders_no)"
        price.text = "\(artists.price)"
    }
}


class ArtistPromoHeader: UICollectionReusableView {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var controlsView: UIView!
    var player: AVPlayer?
    
    func setupVideo(video: String,insert: Bool = true) {
        
        // load Video
        if video.isEmpty == false {
            let videoURL = URL(string: video)
            if player == nil {
                player = AVPlayer(url: videoURL!)
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = self.controlsView.frame
                videoView.layer.masksToBounds = true
                playerLayer.videoGravity = AVLayerVideoGravity.resize
                (insert) ? self.videoView.layer.insertSublayer(playerLayer, at: 0) : self.videoView.layer.replaceSublayer((videoView.layer.sublayers?[0])!, with: playerLayer)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
    }
    
    @objc func playerDidFinishPlaying() {
        statusButton.setImage(#imageLiteral(resourceName: "SliderReplay"), for: .normal)
    }
    
    @IBAction func handlePromoState(_ sender: UIButton) {
        if let player = self.player {
            if sender.imageView?.image == UIImage(named: "SliderReplay") {
                sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "playSlider"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pauseSlider"), for: .normal)
                    player.play()
                }
            }
        }
    }
}


class ArtistVideoCell: UICollectionViewCell {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var views: UILabel!
    var player: AVPlayer?
    var playerLayer:AVPlayerLayer?
    var delegate: ArtistCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupVideo(video: ProfileVideo,insert: Bool = true) {
        
        // load Video
        if video.video.isEmpty == false {
            if player == nil {
                let videoURL = URL(string: video.video)
                player = AVPlayer(url: videoURL!)
                playerLayer = AVPlayerLayer(player: player)
                playerLayer!.frame = self.videoView.frame
                videoView.layer.masksToBounds = true
                playerLayer!.videoGravity = AVLayerVideoGravity.resize
                self.videoView.layer.insertSublayer(playerLayer!, at: 0)
                // (insert) ? self.videoView.layer.insertSublayer(playerLayer, at: 0) : self.videoView.layer.replaceSublayer((videoView.layer.sublayers?[0])!, with: playerLayer)
            } else {
                if video.video.isEmpty == false {
                    let videoURL = URL(string: video.video)
                    player = AVPlayer(url: videoURL!)
                    playerLayer = AVPlayerLayer(player: player)
                    playerLayer!.frame = self.videoView.frame
                    videoView.layer.masksToBounds = true
                    playerLayer!.videoGravity = AVLayerVideoGravity.resize
                    self.videoView.layer.sublayers?.remove(at: 0)
                    self.videoView.layer.insertSublayer(playerLayer!, at: 0)
                }
                
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        self.thumbnail.kf.setImage(with: URL(string: video.thumb_nail?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
        MOLHLanguage.currentAppleLanguage() == "en" ? (self.videoTitle.text  = video.title_en) : (self.videoTitle.text  = video.title_ar)
        likes.text = "\(video.likes)"
        views.text = "\(video.views)"
    }
    
    @objc func playerDidFinishPlaying() {
        statusButton.setImage(#imageLiteral(resourceName: "replay"), for: .normal)
    }
    
    @IBAction func handleVideoState(_ sender: UIButton) {
        if let player = self.player {
            thumbnail.image = nil
            if sender.imageView?.image == UIImage(named: "replay") {
                sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                self.player?.seek(to: CMTime.zero)
                self.player?.play()
            } else {
                if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                    sender.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                    player.pause()
                } else {
                    sender.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                    player.play()
                }
            }
        }
        
    }
    
    @IBAction func openVideoInFullScreen(_ sender: UIButton) {
        delegate?.fullScreenPressed(cell: self)
    }
}


protocol ArtistCellDelegate: class {
    func fullScreenPressed(cell: ArtistVideoCell)
}
