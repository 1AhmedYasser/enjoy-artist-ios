//
//  VideoCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/17/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit
import AVKit
import Kingfisher
import AVFoundation
class VideoCell: UICollectionViewCell {
    
    @IBOutlet weak var videoCategory: DesignableButton!
    @IBOutlet weak var videoLikes: UILabel!
    @IBOutlet weak var videoViews: UILabel!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var artistImage: DesignableImageView!
    
    @IBOutlet weak var videoPlayer: UIView!
    
    var delegate: VideoCellDelegate?
    var player: AVPlayer?
    var video: VideosData?
    var isLiked: Bool = false
    
    let controlsView: UIView = {
        let controls = UIView()
        controls.backgroundColor = UIColor(white: 0, alpha: 0)
        return controls
    }()
    
    let pauseButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "play"), for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return button
    }()
    
    let thumbnail: UIImageView = {
        let thumbnail = UIImageView()
        thumbnail.contentMode = .scaleToFill
        thumbnail.translatesAutoresizingMaskIntoConstraints = false
        return thumbnail
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pauseButton.addTarget(self, action: #selector(changeStatus), for: .touchUpInside)
    }
    
    func setupVideo(video: VideosData) {
        
        videoLikes.text = "\(video.likes)"
        videoViews.text = "\(video.views)"
        videoTitle.text = "\(video.title)"
        artistName.text = "\(video.artist.name)"
        videoCategory.setTitle(video.category.name, for: .normal)
        (video.liked) ? likesButton.setImage(#imageLiteral(resourceName: "thumbs_up_filled"), for: .normal) : likesButton.setImage(#imageLiteral(resourceName: "thumbsUp"), for: .normal)
        isLiked = video.liked
        setEnjoyImage(thumbnail,video.thumb_nail ?? "")
        setEnjoyImage(artistImage,video.artist.image ?? "")
        self.video = video
        
        // load Video
//                if video.video?.isEmpty == false {
//                let videoURL = URL(string: video.video!)
//                player = AVPlayer(url: videoURL!)
//
//                let playerLayer = AVPlayerLayer(player: player)
//                playerLayer.frame = self.videoPlayer.frame
//                playerLayer.videoGravity = AVLayerVideoGravity.resize
//                self.videoPlayer.layer.addSublayer(playerLayer)
//                }
        controlsView.frame = self.videoPlayer.frame
        self.videoPlayer.addSubview(controlsView)
        controlsView.addSubview(thumbnail)
        controlsView.addSubview(pauseButton)
        thumbnail.centerXAnchor.constraint(equalTo: videoPlayer.centerXAnchor).isActive = true
        thumbnail.centerYAnchor.constraint(equalTo: videoPlayer.centerYAnchor).isActive = true
        thumbnail.widthAnchor.constraint(equalToConstant: videoPlayer.bounds.width).isActive = true
        thumbnail.heightAnchor.constraint(equalToConstant: videoPlayer.bounds.height).isActive = true
        pauseButton.centerXAnchor.constraint(equalTo: videoPlayer.centerXAnchor).isActive = true
        pauseButton.centerYAnchor.constraint(equalTo: videoPlayer.centerYAnchor).isActive = true
        pauseButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        pauseButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    @objc func changeStatus() {
        if let player = self.player {
            thumbnail.image = nil
            if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                pauseButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
                player.pause()
            } else {
                pauseButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
                player.play()
            }
        }
    }
    
    @IBAction func handleVideoLikes(_ sender: UIButton) {
        delegate?.likeVideo(cell: self)
    }
    
    @IBAction func showVideoInFullScreen(_ sender: Any) {
        //delegate?.fullScreenPressed(cell: self)
        delegate?.openMediaDetails(cell: self)
    }
    
    func setEnjoyImage(_ imageView: UIImageView,_ url: String) {
        imageView.kf.setImage(with: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? url),options: [.scaleFactor(UIScreen.main.scale),.transition(.fade(1)),.cacheOriginalImage])
    }
}

class CategoryCell: UICollectionViewCell {
    
    
    @IBOutlet weak var categoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setup(_ categoryTitle: String) {
        categoryName.text = categoryTitle
    }
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected
            {
                self.backgroundColor = #colorLiteral(red: 0.716593504, green: 0.3097704053, blue: 0.5101804137, alpha: 1)
            }
            else
            {
                self.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }
        }
    }
}

protocol VideoCellDelegate: class {
    func fullScreenPressed(cell: VideoCell)
    func openMediaDetails(cell: VideoCell)
    func likeVideo(cell: VideoCell)
}
