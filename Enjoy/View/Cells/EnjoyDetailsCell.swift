//
//  AboutUsCell.swift
//  Enjoy
//
//  Created by Ahmed yasser on 12/15/19.
//  Copyright © 2019 Ahmed yasser. All rights reserved.
//

import UIKit

class EnjoyDetailsCell: UITableViewCell {

    @IBOutlet weak var aboutUsText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupCell (text : String) {
        aboutUsText.text = text
       }
}
